.. _background:

========
Overview
========

The section  **Background/Explanations** provides information on key concepts behind Pyxel,
covering topics such as Pyxel's :ref:`architecture <architecture>`,
:ref:`input configuration files <yaml>` and the :ref:`running modes <running_modes>`.
Use this section if you want to learn more about how Pyxel works
and what different :ref:`running modes <running_modes>` can be used for.
Further information on specific parts of the code can be found in the :ref:`Reference<reference>` section.

* :doc:`architecture`
* :doc:`yaml`
* :doc:`detectors`
* :doc:`pipeline`
* :doc:`running_modes`
