# Pyxel additional requirements for generating documentation

sphinx>=5
sphinx-book-theme>=1
pydata-sphinx-theme==0.13.1
sphinxcontrib-bibtex
sphinx-panels
ipython != 8.1; python_version < '3.9'
ipython>=8.1; python_version >= '3.9'
sphinx-inline-tabs
myst-nb
sphinx-copybutton
faqtory
